//
//  MainView.h
//  CallsignChecker
//
//  Created by Marvin Vinluan on 8/1/17.
//  Copyright © 2017 Marvin Vinluan. All rights reserved.
//

#import <UIKit/UIKit.h>

#ifndef MainView_h
#define MainView_h

@interface MainView : UIView <UITextFieldDelegate>

//@property (nonatomic, strong) UITextField *callsignField;
@property (strong, nonatomic) IBOutlet UITextField *callsignField;
@property (strong, nonatomic) IBOutlet UILabel *testLabel;
@property (strong, nonatomic) IBOutlet UILabel *statusLabel;
@property (strong, nonatomic) NSArray *regionLabels;

@property (nonatomic) int testCounter;

@end


#endif /* MainView_h */
