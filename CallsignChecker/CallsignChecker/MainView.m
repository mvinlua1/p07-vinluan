//
//  MainView.m
//  CallsignChecker
//
//  Created by Marvin Vinluan on 8/1/17.
//  Copyright © 2017 Marvin Vinluan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MainView.h"

@implementation MainView

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        self.testCounter = 0;
        [self.callsignField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
        //self.testLabel.text = @"asdf";
        //[self.testLabel setText:@"Enter a callsign"];
        //[self.statusLabel setText:@""];
        self.regionLabels = @[@"Region 0", @"Region 1 (New England)", @"Region 2 (New York/New Jersey)", @"Region 3 (PA, MD, DE)", @"Region 4 (VA, NC, SC, GA, FL, KY, TN, AL)", @"Region 5 (TX, NM, OK, AR, LA, MS)", @"Region 6 (California)", @"Region 7 (WA, OR, NV, AZ, ED, UT, MT, WY)", @"Region 8 (MI, OH, WV)", @"Region 9 (WI, IL, IN)"];
    }
    return self;
}

-(void)textFieldDidChange :(UITextField *) textField{
    //your code
    //self.testCounter++;
    //[self.testLabel setText:[NSString stringWithFormat: @"%d", self.testCounter]];
}

- (BOOL)textField:(UITextField *)textField
shouldChangeCharactersInRange:(NSRange)range
replacementString:(NSString *)string {
    // asks delegate (this class), is it ok to change (add) this text
    BOOL ret = NO;
    // only allow uppercase letters and numerals to be entered
    NSCharacterSet *validChars = [NSCharacterSet characterSetWithCharactersInString:@"ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"];
    NSCharacterSet *invalidChars = [validChars invertedSet];
    NSRange foundRange = [string rangeOfCharacterFromSet:invalidChars];
    if(foundRange.location == NSNotFound) {
        ret = YES;
    }
    return ret;
}

- (BOOL)verifySuffix:(NSString *)textFieldText invalidCharRange:(NSCharacterSet *)rangeOfInvalidChars {
    BOOL verified = NO;
    NSRange foundRange = [textFieldText rangeOfCharacterFromSet:rangeOfInvalidChars];
    if(foundRange.location == NSNotFound) {
        verified = YES;
    }
    return verified;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField {
    [self.testLabel setText:@""];
    [self.statusLabel setText:@"--"];
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    BOOL validated = NO;
    NSUInteger textLength = textField.text.length;
    
    int regionNumber = -1;
    // validate text, display message
    if(textLength >= 4 && textLength <= 6) { // check for length
        unichar character0 = [textField.text characterAtIndex:0];
        NSCharacterSet *numerals = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        NSCharacterSet *nonNumerals = [numerals invertedSet];
        NSRange numeralPosition1 = [textField.text rangeOfCharacterFromSet:nonNumerals options:0 range:NSMakeRange(1, 1)]; // numeral at position 1 (2nd)
        NSRange numeralPosition2 = [textField.text rangeOfCharacterFromSet:nonNumerals options:0 range:NSMakeRange(2, 1)]; // numeral at position 2 (3rd)
        BOOL numeralAt1 = (numeralPosition1.location == NSNotFound);
        BOOL numeralAt2 = (numeralPosition2.location == NSNotFound);
        if(numeralAt1 && !numeralAt2 && textLength < 6) {
            //validated = YES;
            // check that character at pos 0 is K, N, W
            
            if(character0 == 'K' || character0 == 'N' || character0 == 'W') {
                regionNumber = [[textField.text substringWithRange:NSMakeRange(1,1)] integerValue];
                NSString *suffix = [textField.text substringFromIndex:2];
                /*
                NSRange numInSuffix = [suffix rangeOfCharacterFromSet:numerals];
                if(numInSuffix.location == NSNotFound) {
                    [self.statusLabel setText:[NSString stringWithFormat:@"%d", regionNumber]];
                    validated = YES;
                }
                 */
                if([self verifySuffix:suffix invalidCharRange:numerals]) {
                    //[self.statusLabel setText:[NSString stringWithFormat:@"%d", regionNumber]];
                    [self.statusLabel setText:self.regionLabels[regionNumber]];
                    validated = YES;
                }
            }
        } else if(!numeralAt1 && numeralAt2) {
            BOOL passedFirstStep = NO;
            if(character0 == 'A') {
                // handle special
                NSCharacterSet *validAfterA = [NSCharacterSet characterSetWithCharactersInString:@"ABCDEFGHIJKL"];
                NSCharacterSet *invalidAfterA = [validAfterA invertedSet];
                NSRange positionAfterA = [textField.text rangeOfCharacterFromSet:invalidAfterA options:0 range:NSMakeRange(1, 1)];
                if(positionAfterA.location == NSNotFound) {
                    passedFirstStep = YES;
                }

            } else if(character0 == 'K' || character0 == 'W') {
                // handle otherwise
                //regionNumber = [[textField.text substringWithRange:NSMakeRange(2,1)] integerValue];
                //[self.statusLabel setText:[NSString stringWithFormat:@"%d", regionNumber]];
                //validated = YES;
                passedFirstStep = YES;
            } else if(character0 == 'N' && textLength < 6) {
                passedFirstStep = YES;
            }
            if(passedFirstStep) {
                //
                NSString *suffix = [textField.text substringFromIndex:3];
                if([self verifySuffix:suffix invalidCharRange:numerals]) {
                    regionNumber = [[textField.text substringWithRange:NSMakeRange(2,1)] integerValue];
                    //[self.statusLabel setText:[NSString stringWithFormat:@"%d", regionNumber]];
                    [self.statusLabel setText:self.regionLabels[regionNumber]];
                    validated = YES;
                }
            }
        }
    }
    //textField.text;
    if(validated) {
        //
        [self.testLabel setTextColor:[UIColor greenColor]];
    } else {
        //
        [self.testLabel setTextColor:[UIColor redColor]];
        [self.statusLabel setText:@"This does not appear to be a valid US Amateur Radio callsign."];
    }
    //self.testCounter+=2;
    [self.testLabel setText:[NSString stringWithString:textField.text]];
    
    return YES;
}

@end
