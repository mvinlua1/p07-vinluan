//
//  AppDelegate.h
//  CallsignChecker
//
//  Created by Marvin Vinluan on 8/1/17.
//  Copyright © 2017 Marvin Vinluan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

