# Callsign Checker (Project 7)
Marvin Vinluan (mvinlua1@binghamton.edu)  
25 August 2017

Uses a text field to receive a string that may or may not be a valid personal US amateur radio call sign.  The FCC is assigned certain alphanumeric blocks by the ITU (part of the UN), and assigns different formats to different services (four-letter callsigns, for example, belong to broadcast radio and TV stations).  Amateur radio ("ham radio") licenses are generally 4 to 6 characters long, with a single numeral, and begin with A, K, N, or W.  There are a few other range and format exceptions and restrictions, and I've tried to reflect as many as possible in the logic of this program.

The app also displays the region associated with the numeral in each callsign, if it is a valid US amateur radio callsign.

Input is restricted to numerals 0-9 and uppercase letters only.

Examples of correct US amateur callsigns: W1AW, NR5M, AG1RL, WS2BU, WB6NOA
Examples of things that are not US amateur callsigns: N3ABCD, W215AB, KDKA, C6AMI

Primarily using this to get experience working with delegation and string processing in Objective-C as it relates to user interface components like UITextField.

References:

https://code.tutsplus.com/tutorials/ios-sdk-uitextfield-uitextfielddelegate--mobile-10943
https://stackoverflow.com/questions/19348104/check-objective-c-string-for-specific-characters
https://stackoverflow.com/questions/11193611/get-a-char-from-nsstring-and-convert-to-int